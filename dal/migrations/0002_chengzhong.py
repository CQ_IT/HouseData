# -*- coding: utf-8 -*-
# Generated by Django 1.11.20 on 2020-03-12 01:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('dal', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChengZhong',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('position', models.CharField(max_length=255)),
                ('detail', models.CharField(max_length=255)),
                ('price', models.CharField(max_length=255)),
                ('time', models.DateField(auto_now=True)),
            ],
            options={
                'verbose_name': '城中',
                'verbose_name_plural': '城中',
                'db_table': 'ChengZhong',
            },
        ),
    ]
