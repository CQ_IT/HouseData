from django.contrib import admin

# Register your models here.
from dal import models


# Register your models here.

class UserInfoAdmin(admin.ModelAdmin):
    list_display = ('username', 'password', 'create_time')
    list_filter = ['username', 'create_time']


#
admin.site.register(models.UserInfo, UserInfoAdmin)


class ChengXiAdmin(admin.ModelAdmin):
    list_display = ('title', 'position', 'detail', 'price', 'time')
    list_filter = ['title', 'price', 'time']


#
admin.site.register(models.ChengXi, ChengXiAdmin)


class ChengDongAdmin(admin.ModelAdmin):
    list_display = ('title', 'position', 'detail', 'price', 'time')
    list_filter = ['title', 'price', 'time']


#
admin.site.register(models.ChengDong, ChengDongAdmin)


class ChengZhongAdmin(admin.ModelAdmin):
    list_display = ('title', 'position', 'detail', 'price', 'time')
    list_filter = ['title', 'price', 'time']


#
admin.site.register(models.ChengZhong, ChengZhongAdmin)


class ChengBeiAdmin(admin.ModelAdmin):
    list_display = ('title', 'position', 'detail', 'price', 'time')
    list_filter = ['title', 'price', 'time']


#
admin.site.register(models.ChengBei, ChengBeiAdmin)
