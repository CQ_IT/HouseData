from django.db import models


# Create your models here.
class UserInfo(models.Model):
    """
    用户信息表
    """
    username = models.CharField(max_length=32, unique=True)
    password = models.CharField(max_length=64)
    create_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = "用户信息"
        verbose_name_plural = verbose_name
        db_table = 'UserInfo'


class ChengXi(models.Model):
    """
    ChengXi
    """
    title = models.CharField(max_length=255)  # 楼盘
    position = models.CharField(max_length=255)  # 位置
    detail = models.CharField(max_length=255)  # 房屋情况
    price = models.CharField(max_length=255)  # 均价
    time = models.DateField(auto_now=True)  # 数据爬取时间

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "城西"
        verbose_name_plural = verbose_name
        db_table = 'ChengXi'


class ChengZhong(models.Model):
    """
    ChengZhong
    """
    title = models.CharField(max_length=255)  # 楼盘
    position = models.CharField(max_length=255)  # 位置
    detail = models.CharField(max_length=255)  # 房屋情况
    price = models.CharField(max_length=255)  # 均价
    time = models.DateField(auto_now=True)  # 数据爬取时间

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "城中"
        verbose_name_plural = verbose_name
        db_table = 'ChengZhong'


class ChengDong(models.Model):
    """
    ChengDong
    """
    title = models.CharField(max_length=255)  # 楼盘
    position = models.CharField(max_length=255)  # 位置
    detail = models.CharField(max_length=255)  # 房屋情况
    price = models.CharField(max_length=255)  # 均价
    time = models.DateField(auto_now=True)  # 数据爬取时间

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "城东"
        verbose_name_plural = verbose_name
        db_table = 'ChengDong'


class ChengBei(models.Model):
    """
    ChengBei
    """
    title = models.CharField(max_length=255)  # 楼盘
    position = models.CharField(max_length=255)  # 位置
    detail = models.CharField(max_length=255)  # 房屋情况
    price = models.CharField(max_length=255)  # 均价
    time = models.DateField(auto_now=True)  # 数据爬取时间

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "城北"
        verbose_name_plural = verbose_name
        db_table = 'ChengBei'
