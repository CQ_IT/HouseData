import csv
import datetime
import os

import numpy as np


def read_file(path):
    with open(path, 'r', encoding='UTF-8') as f:
        reader = csv.reader(f)
        data = np.array([[]])
        i = 0
        for row in reader:
            if i == 0:
                i = 1
                continue
            x = '%s-%s' % (row[0], row[1])
            y = row[2]
            data.append([changeDate(x), y])
    return data


def changeDate(old):
    source = '2000-1'
    source = datetime.datetime.strptime(source, '%Y-%m')
    old = datetime.datetime.strptime(old, '%Y-%m')

    c = old - source
    days = c.days
    return days


def qeuation():
    file_path = os.path.join("HouseData", "data", "data.csv")
    # 载入数据
    data = np.genfromtxt(file_path, delimiter=",")
    # data = read_file('data/data1.csv')
    # print(data)
    x_data = data[:, 0, np.newaxis]
    y_data = data[:, 1, np.newaxis]
    # 给样本添加偏执项, concatenata是合并两个矩阵, ones创建一个值都为1的矩阵
    X_data = np.concatenate((np.ones((x_data.shape[0], 1)), x_data), axis=1)

    # 方程求解
    def weights(xArr, yArr):
        # 将array格式数据转换为mat格式,mat表示矩阵的数据格式，能进行矩阵运算
        xMat = np.mat(xArr)
        yMat = np.mat(yArr)
        xTx = xMat.T * xMat  # 矩阵乘法，得到公式前半部分
        # 计算矩阵的值，如果值为0，说明该矩阵没有逆矩阵
        if np.linalg.det(xTx) == 0.0:
            print('此矩阵不可逆')
            return
        # 带入公式求解, .T 表示置换矩阵，.I表示逆矩阵
        w = xTx.I * xMat.T * yMat
        return w

    w = weights(X_data, y_data)
    return w


# 画图
# x_test = np.array([[7560], [7680]])
# y_test = w[0] + x_test*w[1]

# plt.plot(x_data, y_data, 'b.')
# plt.plot(x_test, y_test, 'r')
# plt.show()


test_data = ['2020', '8']

# 读取数据生成算法模型
ws = qeuation()


def method(w, data):
    date = '%s-%s' % (data[0], data[1])

    date = changeDate(date)

    x_data = np.array([[date]])

    res = w[0] + x_data * w[1]

    return res[0, 0]


res = method(ws, test_data)
print(res)
