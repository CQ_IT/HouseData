"""HouseData URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from api import register, login, chengxi_data, chengzhong_data, chengdong_data, chengbei_data, yuce
from api.requests_message import chengxi_message, chengzhong_message, chengdong_message, chengbei_message, \
    analyze_message
from api.route import href_index, href_login, href_register, href_chengxi, href_chengzhong, href_chengdong, \
    href_chengbei, href_update, href_analyze, href_yuce

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # 首页
    url('index/$', href_index.TraveIndex.as_view()),
    # 登录跳转
    url('login/$', href_login.Login.as_view()),
    # 注册跳转
    url('register/$', href_register.Register.as_view()),
    # 注册接口
    url('register/create/$', register.Register.as_view()),
    # 注册接口
    url('login/auth/$', login.Login.as_view()),

    # 城西  数据更新接口
    url('get/chengxi/data/$', chengxi_message.RequestsChenxi.as_view()),
    # 城西  网页跳转
    url('chengxi/$', href_chengxi.ChengXi.as_view()),
    # 城西  数据请求接口
    url('show/xi/$', chengxi_data.ChengXi.as_view()),

    # 城中  数据更新接口
    url('get/chengzhong/data/$', chengzhong_message.RequestsChenZhong.as_view()),
    # 城中  网页跳转
    url('chengzhong/$', href_chengzhong.ChengZhong.as_view()),
    # 城中  数据请求接口
    url('show/zhong/$', chengzhong_data.ChengZhong.as_view()),

    # 城东  数据更新接口
    url('get/chengdong/data/$', chengdong_message.RequestsChengDong.as_view()),
    # 城东  网页跳转
    url('chengdong/$', href_chengdong.ChengDong.as_view()),
    # 城东  数据请求接口
    url('show/dong/$', chengdong_data.ChengDong.as_view()),

    # 城北  数据更新接口
    url('get/chengbei/data/$', chengbei_message.RequestsChengBei.as_view()),
    # 城北  网页跳转
    url('chengbei/$', href_chengbei.ChengBei.as_view()),
    # 城北  数据请求接口
    url('show/bei/$', chengbei_data.ChengBei.as_view()),

    # 更新接口
    url('update/$', href_update.Update.as_view()),

    # 分析结果接口
    url('analyze/get/$', analyze_message.AnalyzeMessage.as_view()),
    # 数据分析页面跳转
    url("href/analyze/$", href_analyze.Analyze.as_view()),

    # 预测跳转接口
    url("href/yuce/$", href_yuce.YuCe.as_view()),
    # 数据更新接口
    url("analyze/yuce/$", yuce.YuCe.as_view())
]
