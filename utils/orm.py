import os


def orm_standby():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ComputerJob.settings")  # manage.py文件中有同样的环境配置
    import django
    django.setup()
