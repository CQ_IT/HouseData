from django.shortcuts import render
from rest_framework.views import APIView


class Analyze(APIView):
    def get(self, request):
        return render(request, "analyze.html")
