from django.shortcuts import render
from rest_framework.views import APIView


# 注册
class Register(APIView):
    def get(self, request):
        return render(request, "register.html")
