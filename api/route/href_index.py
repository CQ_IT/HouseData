from django.shortcuts import render
from rest_framework.views import APIView


# 数据首页
class TraveIndex(APIView):
    def get(self, request):
        return render(request, "index.html")
