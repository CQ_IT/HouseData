from django.http import JsonResponse
from django.shortcuts import render, redirect, HttpResponse
from rest_framework.views import APIView

from dal import models


class Login(APIView):
    def get(self, request):
        return render(request, "login.html")

    def post(self, request):
        username = str(request.data.get("username"))
        password = str(request.data.get("password"))
        message = {}
        # 认证账号密码
        user = models.UserInfo.objects.filter(username=username, password=password).first()
        if user:
            request.session['username'] = username
            message['code'] = 200
            message['message'] = "登录成功"
            return JsonResponse(message)
        else:
            message['code'] = 444
            message['message'] = "登录失败"
            return JsonResponse(message)
