from django.http import JsonResponse
from django.shortcuts import render, redirect, HttpResponse
from rest_framework.views import APIView

from dal import models


class Register(APIView):

    def get(self, request):
        return render(request, "login.html")

    def post(self, request):
        username = str(request.data.get("register_username"))
        password = str(request.data.get("register_password"))
        message = {}
        print(username, password)
        try:
            models.UserInfo.objects.create(username=username, password=password)
            message['code'] = 200
            message['message'] = "注册成功"
            return JsonResponse(message)
        except Exception as e:
            print(e)
            message['code'] = 444
            message['message'] = "注册失败"
            return JsonResponse(message)
