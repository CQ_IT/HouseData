from lxml import etree

from utils import orm
from utils import requests_pro

orm.orm_standby()
from rest_framework.views import APIView
from django.http import JsonResponse


class AnalyzeMessage(APIView):
    def post(self, request):

        position = str(request.data.get("position"))  # 位置
        price = str(request.data.get("price"))  # 价格

        message = {}
        try:
            session = requests_pro.session_requests_b()
            print("url", "https://xn.fang.anjuke.com/loupan/{}/{}/".format(position, price))
            # 请求
            page = session.post(url="https://xn.fang.anjuke.com/loupan/{}/{}/".format(position, price))

            # 解码
            page.encoding = page.apparent_encoding
            # 转码
            page_text = page.text

            tree = etree.HTML(page_text)

            # models.ChengBei.objects.all().delete()
            # 获取列表
            li_list = tree.xpath('//*[@id="container"]/div[2]/div[1]/div[3]/div')[0:1]
            message = {}
            dict_data = {}
            for li in li_list:

                try:
                    title = li.xpath('./div/a[1]/span/text()')[0]  # 楼盘
                    print("title", title)
                    position = li.xpath('./div/a[2]/span/text()')[0]  # 位置

                    # 房屋情况
                    span_list = li.xpath('./div/a[3]/span')
                    detail = ""
                    for span in span_list:
                        detail = detail + '|' + span.xpath('./text()')[0]

                    if len(detail) == 0:
                        detail = li.xpath('./div/a[3]/text()')[0]

                    price = li.xpath('./a[2]/p/span/text()')[0]  # 均价

                    try:
                        url = tree.xpath('//*[@id="container"]/div[2]/div[1]/div[2]/div/div/div/a[1]/@href')[0]
                    except:
                        url = tree.xpath('//*[@id="container"]/div[2]/div[1]/div[3]/div/a[1]/@href')[0]

                    dict_data['title'] = title
                    dict_data['position'] = position
                    dict_data['detail'] = detail
                    dict_data['price'] = price
                    dict_data['url'] = url

                    # print("title", title,position,detail,price)

                except Exception as e:
                    print(e)
                    pass
            print(dict_data)
            message['code'] = 200
            message['message'] = dict_data
            return JsonResponse(message)
        except Exception as e:
            print(e)
            message['code'] = 444
            message['message'] = "更新失败"
            return JsonResponse(message)
