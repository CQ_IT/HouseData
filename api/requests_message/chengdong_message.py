import re
import requests
from lxml import etree

from utils import orm
from utils import requests_pro

orm.orm_standby()

import time
from rest_framework.views import APIView
from django.shortcuts import render, redirect, HttpResponse
from dal import models
from django.http import JsonResponse


class RequestsChengDong(APIView):
    def get(self, request):

        message = {}
        try:
            session = requests_pro.session_requests_b()

            # 请求
            page = session.post(url="https://xn.fang.anjuke.com/loupan/chengdongqu/", )
            # 解码
            page.encoding = page.apparent_encoding
            # 转码
            page_text = page.text

            tree = etree.HTML(page_text)

            models.ChengDong.objects.all().delete()
            # 获取列表
            li_list = tree.xpath('//*[@id="container"]/div[2]/div[1]/div[3]/div')[0:10]

            for li in li_list:

                try:
                    title = li.xpath('./div/a[1]/span/text()')[0]  # 楼盘

                    position = li.xpath('./div/a[2]/span/text()')[0]  # 位置

                    # 房屋情况
                    span_list = li.xpath('./div/a[3]/span')
                    detail = ""
                    for span in span_list:
                        detail = detail + '|' + span.xpath('./text()')[0]

                    if len(detail) == 0:
                        detail = li.xpath('./div/a[3]/text()')[0]

                    price = li.xpath('./a[2]/p/span/text()')[0]  # 均价

                    # print("title", title,position,detail,price)
                    # 存入数据库
                    models.ChengDong.objects.create(title=title, position=position, detail=detail, price=price)

                except Exception as e:
                    print(e)
                    pass
            message['code'] = 200
            message['message'] = "更新成功"
            return JsonResponse(message)
        except Exception as e:
            print(e)
            message['code'] = 444
            message['message'] = "更新失败"
            return JsonResponse(message)
