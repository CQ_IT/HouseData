from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework.views import APIView

from HouseData.equation_method import *


def read_file(path):
    with open(path, 'r', encoding='UTF-8') as f:
        reader = csv.reader(f)
        data = []
        i = 0
        for row in reader:
            if i == 0:
                i = 1
                continue
            x = '%s-%s' % (row[0], row[1])
            y = row[2]
            data.append({
                "title": x,
                "price": y
            })
    return data


class YuCe(APIView):

    def get(self, request):
        file_path = os.path.join("HouseData", "data", "data1.csv")
        result = read_file(file_path)
        res = method(ws, test_data)
        result.append(
            {
                "title": "2020-6",
                "price": '%.0f' % res
            })

        data = {}
        try:
            data["code"] = 200
            data["data"] = result
            return Response(data)
        except Exception as e:
            print(e)
            data["code"] = 444
            data["message"] = "请求异常"
            return JsonResponse(data)
